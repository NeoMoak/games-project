﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[SerializeField]
	Camera fpsCam;

	public string playerID;
	public float moveSpeed = 5.0f;
	public float mouseSense = 2.0f;
	public float range = 60.0f;
	float rotVert = 0;


	// Use this for initialization
	void Start () {
		string[] ok = Input.GetJoystickNames ();
		Debug.Log (ok.Length);
		if (playerID == "1") {
			for(int i = 0; i< ok.Length; i++){
				Debug.Log (ok[i]);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		float rotHor = Input.GetAxis ("RightJSHor"+playerID) * mouseSense;
		if (Mathf.Abs(rotHor) < 0.5) {rotHor=0;}
		transform.Rotate (0, rotHor, 0);
		float forward = Input.GetAxis ("LeftJSVert"+playerID) * moveSpeed;
		float strafe = Input.GetAxis ("LeftJSHor"+playerID) * moveSpeed;
		if (Mathf.Abs(forward) < 1) {forward=0;}
		if (Mathf.Abs(strafe) < 1) {strafe=0;}
		Vector3 move = new Vector3 (strafe, 0, forward);
		move = transform.rotation * move;
		CharacterController player = GetComponent<CharacterController> ();
		player.SimpleMove (move);

		float vertDiff = Input.GetAxis ("RightJSVert" + playerID);
		if (Mathf.Abs(vertDiff) < 0.02) {vertDiff=0;}
		rotVert -= vertDiff * mouseSense/2;
		rotVert = Mathf.Clamp (rotVert, -range, range);
		fpsCam.transform.localRotation = Quaternion.Euler (rotVert, 0, 0);
		//rotVert = 0;
	}
}
